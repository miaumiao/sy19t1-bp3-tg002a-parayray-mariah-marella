#include "Planet.h"
#include "TutorialApplication.h"
#include <iostream>
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet::~Planet()
{
}

Planet * Planet::createPlanet(SceneManager *sceneManager, float size, ColourValue colour, bool lighting, std::string name, const float r, const int nRings, const int nSegments)
{
	ManualObject * planet = sceneManager->createManualObject();
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");

	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(lighting);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);

	planet->begin(name, RenderOperation::OT_TRIANGLE_LIST);
	planet->colour(colour);

	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0;

	for (int ring = 0; ring <= nRings; ring++)
	{
		float r0 = r * sinf(ring * fDeltaRingAngle);
		float y0 = r * cosf(ring * fDeltaRingAngle);

		for (int seg = 0; seg <= nSegments; seg++)
		{
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			// Adding vertex
			planet->position(x0, y0, z0);
			planet->normal(Vector3(x0, y0, z0).normalisedCopy());
			planet->textureCoord((float)seg / (float)nSegments, (float)ring / (float)nRings);

			if (ring != nRings)
			{
				// each vertex (except the last) has six indicies pointing to it
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex);
				planet->index(wVerticeIndex + nSegments);
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex + 1);
				planet->index(wVerticeIndex);
				wVerticeIndex++;
			}
		};
	}	//Source: http://wiki.ogre3d.org/ManualSphereMeshes

	planet->end();
	
	SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->createChild();
	node->attachObject(planet);

	return new Planet(node);
}

void Planet::light(SceneManager * sceneManager)
{
	Light *pointLight = sceneManager->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.75f));
	pointLight->setSpecularColour(ColourValue(10.0f, 10.0f, 10.0f));	
	pointLight->setAttenuation(3250.0, 1.0, 0.0014, 0.000007);
}

void Planet::update(const FrameEvent & evt)
{
	float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
	float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float newX = (oldX * Math::Cos((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame)) + (oldZ*Math::Sin((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame));
	float newZ = (oldX * -Math::Sin((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame)) + (oldZ*Math::Cos((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame));
	Degree Rotation = mLocalRotationSpeed;
	mNode->yaw(Radian((360 / 60 * Rotation) * evt.timeSinceLastFrame));
	mNode->setPosition(newX + mParent->mNode->getPosition().x, mNode->getPosition().y, newZ + mParent->mNode->getPosition().z);
}

SceneNode & Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = Radian(Degree(speed));
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Radian(Degree(speed));
}

Vector3 Planet::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}