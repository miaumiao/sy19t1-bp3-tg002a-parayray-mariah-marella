#pragma once
#include "TutorialApplication.h"
#include <OgreManualObject.h>	
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
#include <vector>

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode *node);
	~Planet();
	
	static Planet *createPlanet(SceneManager *sceneManager, float size, ColourValue colour, bool lighting, std::string matName, const float r, const int nRings, const int nSegments);

	void light(SceneManager *sceneManager);
	void update(const FrameEvent & evt);
	SceneNode &getNode();

	void setParent(Planet *parent);
	Planet *getParent();
	
	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

private:
	Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
	SceneNode *mNode;
	Planet *mParent;

	std::vector<Planet*> mPlanets;

	Radian mLocalRotationSpeed;
	Radian mRevolutionSpeed;
};

