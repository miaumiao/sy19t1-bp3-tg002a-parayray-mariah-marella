#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character;
class Skill
{
public:
	Skill(string name, int mpCost);
	~Skill();

	string name;
	int mpCost;

	void applyDamage(Character* player, Character* target);
	virtual void use(Character* player, Character* target);

	string getName();
	int getMpCost();

protected:
	string mName;
	int mMpCost;
};

