#include <iostream>
#include <string>
#include "Assassin.h"
#include "Division.h"
#include "Assassinate.h"
#include "BasicAttack.h"
#include "Skill.h"
#include "Character.h"

using namespace std;

Assassin::Assassin(string className) : Division(className)
{
	mClassName = "Wizard";
}

Assassin::~Assassin()
{
}

string Assassin::getClass()
{
	return mClassName;
}

void Assassin::chooseAction(Character * player, Character * target)
{
	int choice = 0;

	cout << R"(What would you like to do?
			[1] Basic attack
			[2] Heal (mp cost: 11) )" << endl;

	cin >> choice;

	if (choice != 1 && choice != 2)
	{
		while (choice != 1 && choice != 2)
		{
			cin >> choice;
		}
	}
	else if (choice == 2 && player->mp >= 11)
	{
	}
	else
	{
		if (player->mp < 11)
		{
			cout << player->getName() << " has no more mp left! \n";
		}
	}
}
