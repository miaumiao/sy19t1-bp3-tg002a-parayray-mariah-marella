#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character;
class Division
{
public:
	Division(string className);
	~Division();

	string className;
	string getClass();

	virtual void chooseAction(Character* player, Character* target);
private:
	string mClassName;
};