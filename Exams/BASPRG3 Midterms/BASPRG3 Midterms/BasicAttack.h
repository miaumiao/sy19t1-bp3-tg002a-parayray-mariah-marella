#pragma once
#include "Skill.h"

class BasicAttack : public Skill
{
public:
	BasicAttack(string name, int mpCost);
	~BasicAttack();

	void use(Character* player, Character* target);
};

