#pragma once
#include <iostream>
#include <string>
#include "Division.h"
#include "BasicAttack.h"
#include "Assassinate.h"

using namespace std;

class Assassin : public Division
{
public:
	Assassin(string className);
	~Assassin();

	string className;
	string getClass();

	void chooseAction(Character* player, Character* target);

private:
	string mClassName;
};

