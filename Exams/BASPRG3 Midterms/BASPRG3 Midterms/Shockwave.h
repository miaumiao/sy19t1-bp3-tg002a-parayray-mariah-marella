#pragma once
#include "Skill.h"

class Shockwave : public Skill 
{
public:
	Shockwave(string name, int mpCost);
	~Shockwave();

	void use(Character* player, Character* target);
};

