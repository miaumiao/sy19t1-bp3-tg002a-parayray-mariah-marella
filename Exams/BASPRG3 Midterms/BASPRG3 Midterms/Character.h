#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Division.h"
#include "Teams.h"
using namespace std;

class Skill;

class Character
{
public:
	Character(string name, int hp, int baseHp, int mp, int dmg, int pow, int vit, int agi, int dex, int randPower, int hitRate, float damageCoefficient, float bonusDmg, Teams* team, Division* classType);
	~Character();

	string name;
	int hp;
	int baseHp;
	int mp;
	int dmg;
	int pow;
	int vit;
	int agi;
	int dex;

	int randPower;
	int hitRate;
	float damageCoefficient;
	float bonusDmg;

	Teams* team;
	Division* classType;

	string getName();

	void turnOrder(Character* player);
	void displayStats(Character* player);
	void attack(Character* player, Character* target);

private:
	int size;
	string mName;

	vector<Character*> party;

	Teams* mTeam;
	Division* mClass;
};

