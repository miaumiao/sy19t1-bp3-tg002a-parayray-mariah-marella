#include "Character.h"
#include "Skill.h"
#include "Teams.h"
#include "Division.h"
#include "BasicAttack.h"
#include "Assassinate.h"
#include "Shockwave.h"
#include "Heal.h"
#include "Warcraft.h"
#include "Diablo.h"

#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"

#include <iostream>
#include <string>
using namespace std;

Character::Character(string name, int hp, int baseHp, int mp, int dmg, int pow, int vit, int agi, int dex, int randPower, int hitRate, float damageCoefficient, float bonusDmg, Teams* team, Division* classType)
{
	mName = name;
	hp = baseHp;
	baseHp = 0;
	mp = 0;
	dmg = 0;
	pow = 0;
	vit = 0;
	agi = 0;
	dex = 0;
	
	randPower = 0;
	damageCoefficient = 0;
	bonusDmg = 0;
	hitRate = 0;

	name = "";
	team = mTeam;
	classType = mClass;

	party.push_back(new Character("marshmarello", 150, 150, 80, 18, 3, 8, 10, 15, 0, 0, 0, 0, new Teams("Warcraft", 0), new Division("Mage")));
	party.push_back(new Character("miao", 200, 200, 60, 20, 5, 9, 11, 10, 0, 0, 0, 0, new Teams("Warcraft", 0), new Division("Warrior")));
	party.push_back(new Character("mr. lepus", 180, 180, 70, 19, 4, 7, 14, 10, 0, 0, 0, 0, new Teams("Warcraft", 0), new Division("Assassin")));
	party.push_back(new Character("rhopalocera", 150, 150, 80, 19, 3, 8, 10, 10, 0, 0, 0, 0, new Teams("Diablo", 0), new Division("Mage")));
	party.push_back(new Character("sanenekochii", 180, 180, 70, 18, 4, 7, 11, 10, 0, 0, 0, 0, new Teams("Diablo", 0), new Division("Assassin")));
	party.push_back(new Character("miau", 200, 200, 60, 20, 5, 9, 12, 10, 0, 0, 0, 0, new Teams("Diablo", 0), new Division("Warrior")));
}

Character::~Character()
{
	if (mTeam != NULL)
		delete mTeam;
	if (mClass != NULL)
		delete mClass;
	delete &party;
	party.clear();
}

string Character::getName()
{
	return mName;
}

void Character::turnOrder(Character * player)
{
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < (6 - i + 1); j++)
		{
			if (player[i].agi > player[j].agi)
			{
				swap(player[i], player[j]);
			}
		}
	}

	for (int i = 0; i < 6; i++) 
	{
		cout << "=========== [ TURN ORDER ] =========== \n";
		cout << "#" << i << "[" << player[i].team << "]	" << player[i].name << endl;
	}
}

void Character::displayStats(Character* player)
{
	Division *classType = new Division("");

	cout << "===================================== \n";
	cout << "Team: WARCRAFT \n" << "-------------- \n";
	for (int i = 0; i < 6; i++)
	{
		if (player[i].team->getTeam() == "Warcraft")
		{
			cout << player[i].name << "	HP: " << player[i].hp << "/" << player[i].baseHp << endl;
		}
	}

	cout << "========================= \n";

	cout << "Team: DIABLO \n" << "------------ \n";
	for (int i = 0; i < 6; i++)
	{
		if (player[i].team->getTeam() == "Diablo")
		{
			cout << player[i].name << "	HP: " << player[i].hp << "/" << player[i].baseHp << endl;
		}
	}
}

void Character::attack(Character* player, Character* target)
{
	bool game = true;
	while (game = true)
	{
		Skill *ability = new Skill("", 0);
		Division *classType = new Division("");
		Warrior *warriorClass = new Warrior("");
		Mage *mageClass = new Mage("");
		Assassin *assassinClass = new Assassin("");

		for (int i = 0; i < 6; i++)
		{
			if (player[i].team->getTeam() == "Warcraft")
			{
				if (player[i].hp <= 0)
				{
					cout << "Team Diablo won! \n" << endl;
					game = false;
				}
			}
			else if (player[i].team->getTeam() == "Diablo")
			{
				if (player[i].hp <= 0)
				{
					cout << "Team Warcraft won! \n" << endl;
					game = false;
				}
			}
		}

		int choice = 0;
		turnOrder(player);

		system("pause");
		system("cls");

		for (int i = 0; i < 6; i++)
		{
			player->displayStats(player);

			cout << "It is " << player[i].name << "'s turn to attack! \n";

			system("pause");
			system("cls");

			player->hitRate = (player[i].dex / target[i].agi) * 100;
			if (player->hitRate < 20)
			{
				player->hitRate = 20;
			}
			else if (player->hitRate > 80)
			{
				player->hitRate = 80;
			}

			if (player[i].hitRate > 30)
			{
				classType->getClass();
				if (player[i].classType->getClass() == "Warrior")
				{
					ability->use(player, target);
					warriorClass->chooseAction(player, target);
				}
				if (player[i].classType->getClass() == "Mage")
				{
					ability->use(player, target);
					mageClass->chooseAction(player, target);
				}
				if (player[i].classType->getClass() == "Assassin")
				{
					ability->use(player, target);
					assassinClass->chooseAction(player, target);
				}
			}
			else
			{
				cout << player[i].name << "missed! \n";
			}

			system("pause");
			system("cls");
		}
	}
}