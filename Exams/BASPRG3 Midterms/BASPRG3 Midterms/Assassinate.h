#pragma once
#include "Skill.h"

class Assassinate : public Skill
{
public:
	Assassinate(string name, int mpCost);
	~Assassinate();

	void use(Character* player, Character* target);
};

