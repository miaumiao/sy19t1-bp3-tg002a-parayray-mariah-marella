#include "Skill.h"
#include "Character.h"
#include "Division.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

Skill::Skill(string name, int mpCost)
{
	mName = name;
	mMpCost = mpCost;
}

Skill::~Skill()
{
}

string Skill::getName()
{
	return mName;
}

int Skill::getMpCost()
{
	return mMpCost;
}


void Skill::applyDamage(Character* player, Character* target)
{
	srand(time(NULL));
	player->damageCoefficient = 0;

	
	if (player->team->getTeam() == "Warrior" &&  player->team->getTeam() == "Assassin" || player-> team->getTeam() == "Assassin" &&  target-> team->getTeam() == "Mage" || player-> team->getTeam() == "Mage" &&  target->team->getTeam() == "Warrior")
	{
		player->bonusDmg = 1.5;
		player->damageCoefficient += 1.5;
	}
	if (player->team->getTeam() == "Warrior" &&  target->team->getTeam() == "Mage" || player->team->getTeam() == "Assassin" &&  target->team->getTeam() == "Warrior" || player->team->getTeam() == "Mage" &&  target->team->getTeam() == "Assassin")
	{
		player->bonusDmg = 0.5;
	}
	else
	{
		player->bonusDmg = 1;
	}

	player->dmg = (player->dmg - target->vit) *player->bonusDmg;

	int minDmg = 0;
	int playerDmg = 0;
	playerDmg = player->dmg;
	minDmg = playerDmg * 0.8;

	player->randPower = rand() % (player->dmg - minDmg) + player->dmg;
	player->dmg = player->randPower * player->damageCoefficient;
}

void Skill::use(Character* player, Character* target)
{
}
