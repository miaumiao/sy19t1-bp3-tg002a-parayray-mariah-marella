#pragma once
#include <iostream>
#include <string>
#include "Division.h"
#include "BasicAttack.h"
#include "Heal.h"

using namespace std;

class Mage : public Division
{
public:
	Mage(string className);
	~Mage();

	string className;
	string getClass();

	void chooseAction(Character* player, Character* target);

private:
	string mClassName;
};


