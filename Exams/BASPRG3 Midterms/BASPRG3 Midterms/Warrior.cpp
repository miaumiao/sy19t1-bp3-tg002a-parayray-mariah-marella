#include "Warrior.h"
#include "Division.h"
#include "Skill.h"
#include "BasicAttack.h"
#include "Shockwave.h"
#include "Character.h"


Warrior::Warrior(string className) : Division (className)
{
	mClassName = className;
}

Warrior::~Warrior()
{
}

string Warrior::getClass()
{
	return mClassName;
}

void Warrior::chooseAction(Character * player, Character * target)
{
	int choice = 0;

	cout << R"(What would you like to do?
			[1] Basic attack
			[2] Shockwave (mp cost: 12) )" << endl;

	cin >> choice;

	if (choice != 1 && choice != 2)
	{
		while (choice != 1 && choice != 2)
		{
			cin >> choice;
		}
	}
	else if (choice == 2 && player->mp >= 12)
	{
		player->mp -= 12;
	}
	else
	{
		if (player->mp < 12)
		{
			cout << player->getName() << " has no more mp left! \n";
		}
	}
}
