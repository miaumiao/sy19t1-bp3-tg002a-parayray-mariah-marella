#include "Mage.h"
#include "Division.h"
#include "Skill.h"
#include "BasicAttack.h"
#include "Heal.h"
#include "Character.h"

Mage::Mage(string className) : Division (className)
{
	mClassName = className;
}

Mage::~Mage()
{
}

string Mage::getClass()
{
	return mClassName;
}

void Mage::chooseAction(Character * player, Character * target)
{
	int choice = 0;

	cout << R"(What would you like to do?
			[1] Basic attack
			[2] Heal (mp cost: 10)" << endl;

	cin >> choice;

	if (choice != 1 && choice != 2)
	{
		while (choice != 1 && choice != 2)
		{
			cin >> choice;
		}
	}
	else if (choice == 2 && player->mp >= 10)
	{
	}
	else
	{
		if (player->mp < 10)
		{
			cout << player->getName() << " has no more mp left! \n";
		}
	}
}
