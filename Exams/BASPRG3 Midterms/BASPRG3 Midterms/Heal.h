#pragma once
#include "Skill.h"

class Heal : public Skill
{
public:
	Heal(string name, int mpCost);
	~Heal();

	void use(Character* player, Character* target);
};

