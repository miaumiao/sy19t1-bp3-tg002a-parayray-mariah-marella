#include "Heal.h"
#include "Character.h"

Heal::Heal(string name, int mpCost) : Skill(name, mpCost)
{
}


Heal::~Heal()
{
}

void Heal::use(Character * player, Character * target)
{
	int lowestHP = 0;
	for (int i = 0; i < 3; i++)
	{
		if (player[i].hp < player[i + 1].hp)
		{
			lowestHP = player[i].baseHp;
		}
	}

	lowestHP = lowestHP * 0.3;

	for (int i = 0; i < 3; i++)
	{
		player[i].hp += lowestHP;
	}
}
