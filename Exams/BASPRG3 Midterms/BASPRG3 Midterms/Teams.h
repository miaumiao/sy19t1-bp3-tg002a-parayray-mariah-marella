#pragma once
#include <iostream>
#include <string>

using namespace std;

class Teams
{
public:
	Teams(string name, int size);
	~Teams();

	string getTeam();

	virtual void getSize(Teams* team);
private:
	string mTeamName;
	int mTeamSize;
};

