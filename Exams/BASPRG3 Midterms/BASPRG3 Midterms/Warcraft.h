#pragma once
#include "Teams.h"

class Warcraft: public Teams
{
public:
	Warcraft(string name, int size);
	~Warcraft();
	
	string name;
	int size;

	void getSize(Warcraft* warcraft);
};

