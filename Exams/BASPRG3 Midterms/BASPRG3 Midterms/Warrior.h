#pragma once
#include <iostream>
#include <string>
#include "Division.h"
#include "BasicAttack.h"
#include "Shockwave.h"

using namespace std;

class Warrior : public Division
{
public:
	Warrior(string className);
	~Warrior();

	string className;
	string getClass();

	void chooseAction(Character* player, Character* target);

private:
	string mClassName;
};

