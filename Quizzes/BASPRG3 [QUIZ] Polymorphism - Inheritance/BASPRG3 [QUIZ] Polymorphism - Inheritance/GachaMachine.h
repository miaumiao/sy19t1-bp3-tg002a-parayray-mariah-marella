#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Character.h"

using namespace std;

class GachaMachine
{
public:
	GachaMachine(int rPulled, int srPulled, int ssrPulled, int bombPulled, int potionPulled, int crystalPulled);
	~GachaMachine();

	void pullGacha(Character *player);

private:
	vector<Item*> mItem;
};

