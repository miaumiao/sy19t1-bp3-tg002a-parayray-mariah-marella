#pragma once
#include <iostream>
#include <string>

using namespace std;

class Item;
class Inventory
{
public:
	Inventory(int rPulled, int srPulled, int ssrPulled, int bombPulled, int potionPulled, int crystalPulled);
	~Inventory();

	int rPulled;
	int srPulled;
	int ssrPulled;
	int bombPulled;
	int potionPulled;
	int crystalPulled;
	
	void displayInventory(Item* item);
};

