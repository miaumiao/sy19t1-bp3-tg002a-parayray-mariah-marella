#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character;
class Item
{
public:
	Item(string name, int rarity);
	~Item();

	virtual void pullItem(Character* player);

	string getName();
	int getRarity();

protected:
	string mName;
	int mRarity;
};

