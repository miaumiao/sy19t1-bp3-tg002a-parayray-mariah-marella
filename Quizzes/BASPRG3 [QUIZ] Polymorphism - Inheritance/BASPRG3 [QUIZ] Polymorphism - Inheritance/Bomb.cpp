#include "Bomb.h"
#include "Character.h"
#include "Inventory.h"

Bomb::Bomb(string name, int rarity) : Item (name, rarity)
{
}


Bomb::~Bomb()
{
}

void Bomb::pullItem(Character * player, Inventory *collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->ssrPulled++;
	player->hp -= 25;
}
