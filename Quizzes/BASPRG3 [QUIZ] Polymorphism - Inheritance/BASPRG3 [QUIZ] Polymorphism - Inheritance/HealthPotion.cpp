#include "HealthPotion.h"
#include "Character.h"
#include "Inventory.h"

HealthPotion::HealthPotion(string name, int rarity) : Item (name, rarity)
{
}


HealthPotion::~HealthPotion()
{
}

void HealthPotion::pullItem(Character * player, Inventory *collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->ssrPulled++;
	player->hp += 30;
}
