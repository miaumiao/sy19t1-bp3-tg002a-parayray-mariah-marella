#pragma once
#include "Item.h"

using namespace std;

class Inventory;
class R : public Item
{
public:
	R(string name, int rarity);
	~R();

	string name;
	int rarity;

	void pullItem(Character *player, Inventory *collect);
};

