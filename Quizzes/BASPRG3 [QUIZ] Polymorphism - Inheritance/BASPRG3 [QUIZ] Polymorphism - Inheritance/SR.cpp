#include "SR.h"
#include "Character.h"
#include "Inventory.h"

SR::SR(string name, int rarity) : Item(name, rarity)
{
}


SR::~SR()
{
}

void SR::pullItem(Character * player, Inventory * collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->srPulled++;
	player->rarityPointsCollected += 10;
}
