#pragma once
#include "Item.h"
#include "Character.h"
#include "Inventory.h"

using namespace std;

class Crystals : public Item
{
public:
	Crystals(string name, int rarity);
	~Crystals();

	void pullItem(Character* player, Inventory *collect);
};

