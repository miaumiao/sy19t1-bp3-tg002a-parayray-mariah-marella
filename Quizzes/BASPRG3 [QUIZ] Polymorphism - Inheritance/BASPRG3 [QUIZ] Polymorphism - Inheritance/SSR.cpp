#include "SSR.h"
#include "Character.h"
#include "Inventory.h"

SSR::SSR(string name, int rarity) : Item (name, rarity)
{
}


SSR::~SSR()
{
}

void SSR::pullItem(Character * player, Inventory * collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->ssrPulled++;
	player->rarityPointsCollected += 50;
}
