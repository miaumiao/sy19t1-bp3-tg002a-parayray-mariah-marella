#include "Crystals.h"
#include "Character.h"
#include "Inventory.h"

Crystals::Crystals(string name, int rarity) : Item (name, rarity)
{
}


Crystals::~Crystals()
{
}

void Crystals::pullItem(Character * player, Inventory* collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->ssrPulled++;
	player->crystalsCollected += 15;
}
