#include <iostream>
#include <string>
#include <vector>
#include "Character.h"
#include "GachaMachine.h"
#include "Inventory.h"
#include "Item.h"

using namespace std;

Character::Character(int hp, int pullCount, int crystalsCollected, int rarityPointsCollected)
{
	hp = 100;
	pullCount = 0;
	crystalsCollected = 100;
	rarityPointsCollected = 0;
}

Character::~Character()
{
}

void Character::displayStats()
{
	cout << "HP:   " << hp << endl;
	cout << "Rarity Points:   " << rarityPointsCollected << endl;
	cout << "Cystal Count: " << crystalsCollected << endl;
	cout << "Pulls:   " << pullCount << endl;
}

bool Character::isPlayable()
{
	if (this->hp > 0 && crystalsCollected > 0)
	{
		return true;
	}
	else if (crystalsCollected >= 100)
	{
		return false;
	}
	else
	{
		return false;
	}
}