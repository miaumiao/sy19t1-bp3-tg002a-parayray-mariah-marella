#pragma once
#include "Item.h"
#include "Character.h"
#include "Inventory.h"

using namespace std;

class SSR : public Item
{
public:
	SSR(string name, int rarity);
	~SSR();

	void pullItem(Character *player, Inventory *collect);
};

