#pragma once
#include "Item.h"

using namespace std;

class Inventory;
class SR : public Item
{
public:
	SR(string name, int rarity);
	~SR();

	void pullItem(Character *player, Inventory *collect);
};

