#include "R.h"
#include "Character.h"
#include "Inventory.h"

R::R(string name, int rarity) : Item (name, rarity)
{
	name = "R";
	rarity = 1;
}


R::~R()
{
}

void R::pullItem(Character* player, Inventory* collect)
{
	cout << "You pulled " << mName << "! \n";
	cout << "You recieved " << mRarity << " Rarity points \n";

	collect->rPulled++;
	player->rarityPointsCollected += rarity;
}
