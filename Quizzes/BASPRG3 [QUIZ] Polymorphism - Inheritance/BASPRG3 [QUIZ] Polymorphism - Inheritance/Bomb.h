#pragma once
#include "Item.h"
#include "Character.h"
#include "Inventory.h"

using namespace std;

class Bomb : public Item
{
public:
	Bomb(string name, int rarity);
	~Bomb();

	void pullItem(Character* player, Inventory *collect);
};

