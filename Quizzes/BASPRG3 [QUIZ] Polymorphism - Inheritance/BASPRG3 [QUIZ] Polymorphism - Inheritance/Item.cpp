#include "Item.h"

Item::Item(string name, int rarity)
{
	mName = name;
	mRarity = rarity;
}

Item::~Item()
{
}

void Item::pullItem(Character * player)
{
}

string Item::getName()
{
	return mName;
}

int Item::getRarity()
{
	return mRarity;
}
