#include "GachaMachine.h"
#include "Item.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "Bomb.h"
#include "Crystals.h"
#include "HealthPotion.h"
#include <time.h>

using namespace std;

GachaMachine::GachaMachine(int rPulled, int srPulled, int ssrPulled, int bombPulled, int potionPulled, int crystalPulled)
{
	rPulled = 0;
	srPulled = 0;
	ssrPulled = 0;
	bombPulled = 0;
	potionPulled = 0;
	crystalPulled = 0;

	mItem.push_back(new R("R", 1));
	mItem.push_back(new SR("SR", 10));
	mItem.push_back(new SSR("SSR", 50));
	mItem.push_back(new Bomb("Bomb", 25));
	mItem.push_back(new HealthPotion("HealthPotion", 30));
	mItem.push_back(new Crystals("Crystals", 15));
}

GachaMachine::~GachaMachine()
{
}

void GachaMachine::pullGacha(Character* player)
{
	srand(time(NULL));
	int randIndex = rand() % 100;
	int itemNo = 0;

	if (randIndex <= 1)
	{
		itemNo = 2;
	}
	else if (randIndex > 1 && randIndex <= 10)
	{
		itemNo = 1;
	}
	else if (randIndex > 10 && randIndex <= 50)
	{
		itemNo = 0;
	}
	else if (randIndex > 50 && randIndex <= 65)
	{
		itemNo = 4;
	}
	else if (randIndex > 65 && randIndex <= 85)
	{
		itemNo = 5;
	}
	else if (randIndex > 85 && randIndex <= 100)
	{
		itemNo = 3;
	}

	Item *item = mItem[randIndex];
	item->pullItem(player);
}