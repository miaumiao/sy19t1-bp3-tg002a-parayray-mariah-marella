#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "GachaMachine.h"
#include "Inventory.h"

using namespace std;

class Character
{
public:
	Character(int hp, int pullCount, int crystalsCollected, int rarityPointsCollected);
	~Character();

	int hp;
	int pullCount;
	int crystalsCollected;
	int rarityPointsCollected; 

	void displayStats();
	bool isPlayable();
};