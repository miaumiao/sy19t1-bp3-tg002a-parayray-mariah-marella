#include <iostream>
#include <string>
#include "Character.h"
#include "GachaMachine.h"
#include "Inventory.h"
#include "Item.h"

using namespace std;

int main()
{
	Character* player = new Character(100, 0, 100, 0);
	Inventory* collected = new Inventory(0, 0, 0, 0, 0, 0);
	GachaMachine* generator = new GachaMachine(0, 0, 0, 0, 0, 0);
	Item* item = new Item("", 0);

	while (player->isPlayable())
	{
		player->displayStats();
		cout << endl;
		
		generator->pullGacha(player);

		player->crystalsCollected -= 5;
		player->pullCount++;		

		system("pause");
		system("cls");
	}

	if (player->crystalsCollected >= 100)
	{
		system("cls");

		cout << "You won! \n";
		cout << "------------------------ \n";
		player->displayStats();
		cout << "------------------------ \n";
		collected->displayInventory(item);
	}

	else
	{
		system("cls");

		cout << "You lost! \n";
		cout << "------------------------ \n";
		player->displayStats();
		cout << "------------------------ \n";
		collected->displayInventory(item);
	}

	system("pause");
	system("cls");
}