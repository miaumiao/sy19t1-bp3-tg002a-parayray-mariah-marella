#include "Inventory.h"
#include "Item.h"

using namespace std;

Inventory::Inventory(int rPulled, int srPulled, int ssrPulled, int bombPulled, int potionPulled, int crystalPulled)
{
	rPulled = 0;
	srPulled = 0;
	ssrPulled = 0;
	bombPulled = 0;
	potionPulled = 0;
	crystalPulled = 0;
}

Inventory::~Inventory()
{
}

void Inventory::displayInventory(Item * item)
{
	if (rPulled > 0)
	{
		cout << "x" << rPulled << " R" << endl;
	}
	if (srPulled > 0)
	{
		cout << "x" << srPulled << " SR" << endl;
	}
	if (ssrPulled > 0)
	{
		cout << "x" << ssrPulled << " SSR" << endl;
	}
	if (bombPulled > 0)
	{
		cout << "x" << bombPulled << " Bomb" << endl;
	}
	if (potionPulled > 0)
	{
		cout << "x" << potionPulled << " Potion" << endl;
	}
	if (crystalPulled > 0)
	{
	cout << "x" << crystalPulled << " Crystal" << endl;
	}
}
