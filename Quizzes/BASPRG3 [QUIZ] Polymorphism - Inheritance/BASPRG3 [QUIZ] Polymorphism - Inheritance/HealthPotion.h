#pragma once
#include "Item.h"
#include "Character.h"
#include "Inventory.h"

class HealthPotion : public Item
{
public:
	HealthPotion(string name, int rarity);
	~HealthPotion();

	void pullItem(Character* player, Inventory* collect);
};

