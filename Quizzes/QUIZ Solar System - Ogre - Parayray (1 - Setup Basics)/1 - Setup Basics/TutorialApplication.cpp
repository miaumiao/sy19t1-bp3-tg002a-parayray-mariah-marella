/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include<OgreManualObject.h>	
#include "Planet.h"
#include <vector>

using namespace Ogre;

TutorialApplication::TutorialApplication(void)
{
}

TutorialApplication::~TutorialApplication(void)
{
}

void TutorialApplication::createScene(void)
{
	Planet* Sun = Planet::createPlanet(mSceneMgr, 10, ColourValue(1, 1, 0));
	Sun->getNode().setPosition(0, 0, 0);
	Sun->setLocalRotationSpeed(1.0);
	Sun->setParent(Sun);

	Planet* Mercury = Planet::createPlanet(mSceneMgr, 1.5, ColourValue(1, 1, 0.5));
	Mercury->getNode().setPosition(31.5, 0, 0);
	Mercury->setLocalRotationSpeed(1.0);
	Mercury->setRevolutionSpeed(1.0 * 4.14);
	Mercury->setParent(Sun);

	Planet* Venus = Planet::createPlanet(mSceneMgr, 2.5, ColourValue(1, 0.5, 0));
	Venus->getNode().setPosition(44, 0, 0);
	Venus->setLocalRotationSpeed(1.0);
	Venus->setRevolutionSpeed(1.0 * 1.62);
	Venus->setParent(Sun);

	Planet* Earth = Planet::createPlanet(mSceneMgr, 5, ColourValue(0, 0.5, 1));
	Earth->getNode().setPosition(89, 0, 0);
	Earth->setLocalRotationSpeed(1.0);
	Earth->setRevolutionSpeed(1.0);
	Earth->setParent(Sun);

	Planet* Moon = Planet::createPlanet(mSceneMgr, 0.5, ColourValue(1, 1, 0.5));
	Moon->setParent(Earth);
	Moon->getNode().setPosition(Moon->getParent()->getNode().getPosition().x + 20, 0, 0);
	Moon->setLocalRotationSpeed(1.0);
	Moon->setRevolutionSpeed(24.0);

	Planet* Mars = Planet::createPlanet(mSceneMgr, 4, ColourValue(0.5, 0.5, 0));
	Mars->getNode().setPosition(138, 0, 0);
	Mars->setLocalRotationSpeed(1.0);
	Mars->setRevolutionSpeed(1.0 * 0.53);
	Mars->setParent(Sun);

	mPlanets.push_back(Sun);
	mPlanets.push_back(Mercury);
	mPlanets.push_back(Venus);
	mPlanets.push_back(Earth);
	mPlanets.push_back(Moon);
	mPlanets.push_back(Mars);
}

bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	for (int i = 0; i < mPlanets.size(); i++)
	{
		mPlanets.at(i)->update(evt);
	}

	return true;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif