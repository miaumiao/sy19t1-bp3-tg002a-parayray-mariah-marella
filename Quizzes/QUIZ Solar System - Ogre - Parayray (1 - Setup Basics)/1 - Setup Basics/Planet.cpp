#include "Planet.h"
#include "TutorialApplication.h"
#include <iostream>
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet::~Planet()
{
}

Planet * Planet::createPlanet(SceneManager *sceneManager, float size, ColourValue colour)
{
	ManualObject *planet = sceneManager->createManualObject();
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	{
		//Front
		planet->position(-size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));
		planet->position(-size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));
		planet->position(size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));

		planet->position(size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));
		planet->position(-size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));
		planet->position(size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, 1));
	}

	{
		//Back
		planet->position(size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));
		planet->position(size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));
		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));

		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));
		planet->position(size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));
		planet->position(-size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 0, -1));
	}

	{
		//Left Side
		planet->position(-size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));
		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));
		planet->position(-size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));

		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));
		planet->position(-size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));
		planet->position(-size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(-1, 0, 0));
	}

	{
		//Right Side
		planet->position(size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));
		planet->position(size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));
		planet->position(size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));

		planet->position(size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));
		planet->position(size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));
		planet->position(size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(1, 0, 0));
	}

	{
		//Top
		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));
		planet->position(-size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));
		planet->position(size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));

		planet->position(size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));
		planet->position(-size, size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));
		planet->position(size, size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, 1, 0));
	}

	{
		//Bottom
		planet->position(-size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));
		planet->position(size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));
		planet->position(size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));

		planet->position(-size, -size, -size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));
		planet->position(size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));
		planet->position(-size, -size, size);
		planet->colour(colour);
		planet->normal(Vector3(0, -1, 0));
	}

	planet->end();

	SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->attachObject(planet);
	node->createChild();

	return new Planet(node);

	return nullptr;
}

void Planet::update(const FrameEvent & evt)
{
	float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
	float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float newX = (oldX * Math::Cos((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame)) + (oldZ*Math::Sin((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame));
	float newZ = (oldX * -Math::Sin((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame)) + (oldZ*Math::Cos((360 / 60 * mRevolutionSpeed) * evt.timeSinceLastFrame));
	Degree Rotation = mLocalRotationSpeed;
	mNode->yaw(Radian((360 / 60 * Rotation) * evt.timeSinceLastFrame));
	mNode->setPosition(newX + mParent->mNode->getPosition().x, mNode->getPosition().y, newZ + mParent->mNode->getPosition().z);
}

SceneNode & Planet::getNode()
{
	// TODO: insert return statement here
	return *mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = Radian(Degree(speed));
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Radian(Degree(speed));
}
