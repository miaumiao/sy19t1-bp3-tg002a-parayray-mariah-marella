#include <iostream>
#include <string>

#include "PokeParty.h"
#include "Pokemon.h"

using namespace std;

PokeParty::PokeParty()
{
	this->partySize[6];
	this->hp[6];
	this->baseHp[6];
	this->level[6];
	this->baseDamage[6];
	this->exp[6];
	this->expToNextLevel[6];
	this->pokemonNo[6];	
}

PokeParty::PokeParty(int hp, int baseHp, int level, int baseDamage, int exp, int expToNextLevel, int pokemonNo, int partySize)
{
	this->partySize[6] = partySize;
	this->hp[6] = hp;
	this->baseHp[6] = baseHp;
	this->level[6] = level;
	this->baseDamage[6] = baseDamage;
	this->exp[6] = exp;
	this->expToNextLevel[6] = expToNextLevel;
	this->pokemonNo[6] = pokemonNo;
}

void PokeParty::savePartyInfo(PokeParty* party, Pokemon * pika)
{
	for (int x = 0; x < 6; x++)
	{
		if (party->partySize[x] == 0)
		{
			party->partySize[x] = pika->pokemonNo;
			party->level[x] = pika->level;
			party->hp[x] = pika->hp;
			party->baseHp[x] = pika->baseHp;
			party->exp[x] = pika->exp;
			party->expToNextLevel[x] = pika->expToNextLevel;
			party->baseDamage[x] = pika->baseDamage;

			break;
		}
		if (party->partySize[5] != 0)
		{
			cout << "Your party is full, caught pokemon is released back to the wild \n" << endl;
		}
	}
}

void PokeParty::healParty(PokeParty * party)
{
	for (int x = 0; x < 6; x++)
	{
		party->hp[x] = party->baseHp[x];

		if (party->partySize[x] == 0)
		{
			break;
		}
	}
}