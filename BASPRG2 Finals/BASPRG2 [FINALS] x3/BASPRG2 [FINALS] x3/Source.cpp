#include <iostream>
#include <string>

#include "Player.h"
#include "Pokemon.h"
#include "PokeParty.h"

using namespace std;

int main()
{
	Player* you = new Player();
	Pokemon* pika = new Pokemon();
	PokeParty* party = new PokeParty();

	you->intro(you);
	pika->chooseStarter(pika, party);

	while (true)
	{
		you->runChoice(you, pika, party);
	}
	system("pause");
	system("cls");
}