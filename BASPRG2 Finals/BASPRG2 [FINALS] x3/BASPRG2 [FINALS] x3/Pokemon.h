#pragma once
#include <string>

using namespace std;

class Player;
class PokeParty;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string name, int hp, int baseHp, int level, int baseDamage, int exp, int expToNextLevel, int pokemonNo);

	string name;
	int hp;
	int baseHp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
	int pokemonNo;

	void chooseStarter(Pokemon* pika, PokeParty * party);

	void displayInfo(Pokemon* pika);
	void pokemonDb(Pokemon* pika);

	void battle(Pokemon* pikaParty, PokeParty* party, Player* you);
	void addToParty(Pokemon*pika, PokeParty* you);
	void pokemonParty(Pokemon* pika, PokeParty* you);

};