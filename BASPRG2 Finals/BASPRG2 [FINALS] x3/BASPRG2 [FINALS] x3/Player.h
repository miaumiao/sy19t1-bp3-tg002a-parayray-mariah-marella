#pragma once
#include <string>
#include "Pokemon.h"

using namespace std;

class PokeParty;

class Player
{
public:
	Player();
	Player(string playerName, int choice, char move, char consent, int x, int y, int locationNo, int pokeball, int money);

	string playerName;
	int choice;
	char move;
	char consent;
	int x;
	int y;
	int locationNo;
	int pokeball;
	int money;

	void intro(Player* you);
	void areaDisplay(Player* you);

	void runChoice(Player* you, Pokemon* pika, PokeParty* party);
};