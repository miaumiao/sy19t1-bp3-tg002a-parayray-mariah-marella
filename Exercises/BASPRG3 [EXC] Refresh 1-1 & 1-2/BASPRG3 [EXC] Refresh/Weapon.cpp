#include <iostream>
#include <time.h>
#include "Character.h"
#include "Weapon.h"

Weapon::Weapon()
{
	this->name;
	this->damage = 20;
}

Weapon::Weapon(string name, int damage)
{
	this->name = name;
	this->damage = damage;
}

void Weapon::Damage(Character * player, Character* player2)
{
	srand(time(NULL));
	int chance = rand() % 10;

	if (player->skill == 0)
	{
		player2->hp -= damage;
	}
	else if (player->skill == 1)
	{
		if (chance > 2)
		{
		player2->hp -= this->damage * 2;
		}
		else
		{
			player2->hp -= this->damage;
		}
	}
	else if (player->skill == 2)
	{
		player2->hp -= this->damage * 2;
		player->hp -= (this->damage * 2) * 0.25;
		player->mp -= 3;
	}
	else if (player->skill == 3)
	{
		player2->hp -= (player->hp * 0.25) * 2 + this->damage;
		player->hp -= player->hp * 0.25;
		player->mp -= 5;

		if (player->hp <= 0)
		{
			player->hp = 1;
		}
	}
	else if (player->skill == 4)
	{
		player2->hp -= this->damage * 2;
		player->mp -= 4;
	}
}
