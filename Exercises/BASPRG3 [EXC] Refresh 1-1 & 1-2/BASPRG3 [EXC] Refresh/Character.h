#pragma once
#include <string>

using namespace std;

class Skill;
class Weapon;
class NormalAttack;
class Syphon;
class Vengeance;
class DropelBlade;

class Character
{
public:
	Character();
	Character(int hp, int mp, int skill);

	int hp;
	int mp;
	int skill;

	void Display(Character* player, Character* enemy);
	void Action(Character* player, Character* enemy, Weapon* item);
	void Skill(Character* player);

private:
	// Add a vector of skills here
	NormalAttack *skill1 = new NormalAttack();
	Vengeance *skill2 = new Vengeance();
	Syphon *skill3 = new Syphon();
	DropelBlade *skill4 = new DropelBlade();
};