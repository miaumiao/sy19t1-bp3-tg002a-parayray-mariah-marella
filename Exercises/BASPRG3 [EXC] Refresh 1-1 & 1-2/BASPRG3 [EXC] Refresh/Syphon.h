#pragma once
#include <iostream>
#include "Skill.h"
using namespace std;

class Syphon : public Skill
{
public:
	Syphon();
	~Syphon();

	void executeSkill(Character *caster, Character *target);
};

