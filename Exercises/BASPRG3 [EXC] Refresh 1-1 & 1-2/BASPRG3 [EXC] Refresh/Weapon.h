#pragma once
#include <string>
#pragma once
#include <string>

using namespace std;

class Skill;
class Weapon;
class NormalAttack;
class Syphon;
class Vengeance;
class DropelBlade;

class Character;

class Weapon
{
public:
	Weapon();
	Weapon(string name,	int damage);

	string name;
	int damage;

	void Damage(Character * player, Character * enemy);
};