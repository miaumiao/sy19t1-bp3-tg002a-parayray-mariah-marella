#pragma once
#include <iostream>
#include "Skill.h"
using namespace std;

class DropelBlade : public Skill
{
public:
	DropelBlade();
	~DropelBlade();

	void executeSkill(Character *caster, Character *target);
};

