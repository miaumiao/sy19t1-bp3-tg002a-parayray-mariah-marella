#include <iostream>
#include <time.h>
#include "Character.h"
#include "Weapon.h"
#include "Skill.h"


Character::Character()
{
	this->hp = 300;
	this->mp = 20;
	this->skill = 0;
}

Character::Character(int hp, int mp, int skill)
{
	this->hp = hp;
	this->mp = mp;
	this->skill = skill;
}

void Character::Display(Character * player, Character * enemy)
{
	cout << "Player" << endl;
	cout << "------" << endl;
	cout << "HP: " << player->hp << endl;
	cout << "MP: " << player->hp << endl;
	cout << endl;

	cout << "Enemy" << endl;
	cout << "------" << endl;
	cout << "HP: " << enemy->hp;
	cout << "MP: " << enemy->hp;
}

void Character::Action(Character* player, Character* enemy, Weapon * item)
{ 
	int baseHp = player->hp;

	cout << "You've been challenged to a battle! \n";
	std::cin.get();

	int randomNum = 0;

	while (player->hp >= 0 && enemy->hp >= 0)
	{
		player->Display(player, enemy);

		randomNum = rand() % 2;
		Skill* randomSkill = randomSkill[randomNum];

		randomSkill->executeSkill(player, enemy);
	}

	while (player->hp >= 0 && enemy->hp >= 0)
	{
		cout << "Player attacks! \n" << "The player ";
		player->Skill(player);
		item->Damage(player, enemy);

		cout << "Enemy's turn! \n" << "The enemy ";
		player->Skill(enemy);
		item->Damage(enemy, player);

		if (player->hp <= 0)
		{
			cout << "You have been defeated! \n";
			break;
		}
		else if (enemy->hp <= 0)
		{
			cout << "You defeated the enemy! \n";
		}
		player->hp = baseHp;

		system("pause");
		system("cls");
	}

	cout << "You died. \n";
}
