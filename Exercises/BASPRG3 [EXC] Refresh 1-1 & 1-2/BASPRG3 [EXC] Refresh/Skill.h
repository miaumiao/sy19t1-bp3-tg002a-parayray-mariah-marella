#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character;
class Weapon;

class Skill
{
public:
	Skill(string name, int mpCost);
	Skill(string name, int mpCost);

	virtual void executeSkill(Character *caster, Character *target);

protected:
	string name;
	int mpCost;
};





//this
class normalAttack : Skill
{

};

class Vengeance : Skill
{

};

class Syphon : Skill
{

};

class DropelBlade : Skill
{

};