#pragma once
#include <iostream>
#include "Skill.h"
using namespace std;

class Vengeance : public Skill
{
public:
	Vengeance();
	~Vengeance();

	void executeSkill(Character *caster, Character *target);
};

