#pragma once
#include <iostream>
#include "Skill.h"
using namespace std;

class NormalAttack : public Skill
{
public:
	NormalAttack();
	~NormalAttack();

	void executeSkill(Character *caster, Character *target);
};

