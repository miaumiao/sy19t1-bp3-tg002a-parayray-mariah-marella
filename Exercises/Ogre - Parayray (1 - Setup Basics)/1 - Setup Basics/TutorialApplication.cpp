/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include<OgreManualObject.h>	

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)

	ManualObject *object = mSceneMgr->createManualObject();

	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	{
		//Back
		object->position(0, 10, 0);
		object->position(10, 0, 0);
		object->position(0, 0, 0);

		object->position(0, 10, 0);
		object->position(10, 10, 0);
		object->position(10, 0, 0);
	}

	{
		//Front
		object->position(0, 10, 10);
		object->position(0, 0, 10);
		object->position(10, 0, 10);

		object->position(10, 10, 10);
		object->position(0, 10, 10);
		object->position(10, 0, 10);
	}

	{
		//Left Side
		object->position(0, 10, 0);
		object->position(0, 0, 0);
		object->position(0, 0, 10);

		object->position(0, 10, 10);
		object->position(0, 10, 0);
		object->position(0, 0, 10);
	}

	{
		//Right Side
		object->position(10, 10, 0);
		object->position(10, 0, 0);
		object->position(10, 0, 10);

		object->position(10, 10, 10);
		object->position(10, 10, 0);
		object->position(10, 0, 10);
	}

	{
		//Top
		object->position(0, 10, 0);
		object->position(0, 10, 10);
		object->position(10, 10, 0);

		object->position(0, 10, 10);
		object->position(10, 10, 10);
		object->position(10, 10, 0);
	}

	{
		//Bottom
		object->position(0, 0, 10);
		object->position(0, 0, 0);
		object->position(10, 0, 0);

		object->position(10, 0, 10);
		object->position(0, 0, 10);
		object->position(10, 0, 0);
	}

	object->end();

	node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(object);
}
//---------------------------------------------------------------------------



bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	int accelerate = 0;
	accelerate = acceleration;

	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		node->translate((1 * evt.timeSinceLastFrame + acceleration), 0, 0);
		acceleration++;
	}

	return true;
}





#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
