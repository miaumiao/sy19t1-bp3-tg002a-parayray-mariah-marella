#include <iostream>
#include <time.h>
#include "Character.h"
#include "Weapon.h"


Character::Character()
{
	this->hp = 300;
	this->mp = 20;
	this->skill = 0;
}

Character::Character(int hp, int mp, int skill)
{
	this->hp = hp;
	this->mp = mp;
	this->skill = skill;
}

void Character::Display(Character * player, Character * enemy)
{
	cout << "Player" << endl;
	cout << "------" << endl;
	cout << "HP: " << player->hp << endl;
	cout << "MP: " << player->hp << endl;
	cout << endl;

	cout << "Enemy" << endl;
	cout << "------" << endl;
	cout << "HP: " << enemy->hp;
	cout << "MP: " << enemy->hp;
}

void Character::Action(Character* player, Character* enemy, Weapon * item)
{
	int baseHp = player->hp;

	cout << "You've been challenged to a battle! \n";
	std::cin.get();

	while (player->hp >= 0 && enemy->hp >= 0)
	{
		cout << "Player attacks! \n" << "The player ";
		player->Battle(player);
		item->Damage(player, enemy);

		cout << "Enemy's turn! \n" << "The enemy ";
		player->Battle(enemy);
		item->Damage(enemy, player);

		if (player->hp <= 0)
		{
			cout << "You have been defeated! \n";
			break;
		}
		else if (enemy->hp <= 0)
		{
			cout << "You defeated the enemy! \n";
		}
		player->hp = baseHp;

		system("pause");
		system("cls");
	}

	cout << "You died. \n";
}

void Character::Battle(Character * player)
{
	//srand(time(NULL));
	int num = rand() % 4;
	player->skill = num;

	if (player->mp > 1)
	{
		if (player->skill == 0)
		{
			cout << "used a Normal Attack /n";
		}
		else if (player->skill == 1)
		{
			cout << "dealt a Critical Attack/n";
		}
		else if (player->skill == 2)
		{
			cout << "used Syphon /n";
		}
		else if (player->skill == 3)
		{
			cout << "used Vengeance /n";
		}
		else if (player->skill == 4)
		{
			cout << "used a Dropel Blade \n" << endl;
			cout << "A clone was summonned! \n" << "Both plunges to an attack! \n";
		}
	}
	else
	{
		if (player->skill == 2 || player->skill == 3)
		{
			cout << "used a Normal Attack /n";
		}
		else if (player->skill == 4)
		{
			cout << "dealt a Critical Attack/n";
		}
	}
}
