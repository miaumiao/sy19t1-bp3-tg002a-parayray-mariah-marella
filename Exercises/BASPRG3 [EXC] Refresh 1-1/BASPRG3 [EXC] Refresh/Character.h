#pragma once
#include <string>

using namespace std;

class Character
{
public:
	Character();
	Character(int hp, int mp, int skill);
	
	int hp;
	int mp;
	int skill;
	
	void Display(Character* player, Character* enemy);
	void Action(Character* player, Character* enemy, Weapon* item);
	void Battle(Character* player);
};