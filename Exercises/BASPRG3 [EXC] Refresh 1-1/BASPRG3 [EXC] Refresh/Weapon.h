#pragma once
#include <string>

using namespace std;

class Character;

class Weapon
{
public:
	Weapon();
	Weapon(string name,	int damage);

	string name;
	int damage;

	void Damage(Character * player, Character * enemy);
};